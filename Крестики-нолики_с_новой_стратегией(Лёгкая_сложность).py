class X_O():

    def __init__(self):
        self.X = "X"
        self.O = "O"
        self.free_space = " "
        self.NOWIN = "Ничья"
        self.square = 9


        self.display_instruct()
        self.pieces()
        self.turn = self.X
        self.board = []
        for square in range(self.square):
            self.board.append(self.free_space)
        self.display_board()
        while not self.winner():
            if self.turn == self.human:
                move = self.human_move()
                self.board[move] = self.human
            else:
                move = self.computer_move()
                self.board[move] = self.computer
            self.display_board()
            self.next_turn()
        the_winner = self.winner()
        self.congrat_winner(the_winner)


    def display_instruct(self):
        print(
            """
            Чтобы сделать ход, введи число от 0 до 8. Каждое число однозначно соответсвуют полям доски:
                                                        0 | 1 | 2
                                                        ---------
                                                        3 | 4 | 5
                                                        ---------
                                                        6 | 7 | 8
            """
        )



    def pieces(self):
        go_first = None
        while go_first not in ("y", "n"):
            go_first = input("Хотите оставить за собой певый ход? (y/n): ").lower()
        if go_first == "y":
            self.human = self.X
            self.computer = self.O
        else:
            self.computer = self.X
            self.human = self.O
            
    def display_board(self):
        print("\n\t", self.board[0], "|", self.board[1], "|", self.board[2])
        print("\t", "---------")
        print("\n\t", self.board[3], "|", self.board[4], "|", self.board[5])
        print("\t", "---------")
        print("\n\t", self.board[6], "|", self.board[7], "|", self.board[8])


    def legal_moves(self):
        moves = []
        for square in range(self.square):
            if self.board[square] == self.free_space:
                moves.append(square)
        return moves


    def winner(self):
        WAYS_TO_WIN = ((0, 1, 2),
                       (3, 4, 5),
                       (6, 7, 8),
                       (0, 3, 6),
                       (1, 4, 7),
                       (2, 5, 8),
                       (0, 4, 8),
                       (2, 4, 6))
        for row in WAYS_TO_WIN:
            if self.board[row[0]] == self.board[row[1]] == self.board[row[2]] != self.free_space:
                winner = self.board[row[0]]
                return winner
            if self.free_space not in self.board:
                return self.NOWIN
        return None


    def human_move(self):
        legal = self.legal_moves()
        move = None
        while move not in legal:
            while move not in range(0, self.square):
                move = int(input("Ваш ход. Выберите одно из полей (0 - 8):"))
            if move not in legal:
                print("\nЭто поле уже занято. Выберите другое.\n")
        return move


    def computer_move(self):
        self.board = self.board[:]
        BEST_MOVES = (7, 5, 3, 1, 8, 6, 2, 0, 4)
        print("Компьтер выбирает", end=" ")
        for move in self.legal_moves():
            self.board[move] = self.computer
            if self.winner() == self.computer:
                print(move)
                return move
            self.board[move] = self.free_space
        for move in self.legal_moves():
            self.board[move] = self.human
            if self.winner() == self.human:
                print(move)
                return move
            self.board[move] = self.free_space
        for move in BEST_MOVES:
            if move in self.legal_moves():
                print(move)
                return move


    def next_turn(self):
        if self.turn == self.X:
            self.turn = self.O
        else:
            self.turn = self.X  


    def congrat_winner(self, the_winner):
        if the_winner == self.computer:
            print("Вы проиграли")
        elif the_winner == self.human:
            print("Вы выиграли")
        elif the_winner == self.NOWIN:
            print(self.NOWIN)

game = X_O()