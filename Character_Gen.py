class Character():
	def __init__(self):
		self.value = 30 
		self.Strengh = 0
		self.Health = 0
		self.Agility = 0
		self.Sapience = 0

	def StrenghAdd(self, a):
		if self.value > 0:
			if a > self.value:
				a = self.value
			self.Strengh += a
			self.value -=a

	def StrenghSub(self, a):
		if a > self.value:
			a = self.value

		if (self.Strengh - a) >= 0:
			self.Strengh -= a
			self.value +=a

	def HealthAdd(self, a):
		if self.value > 0:
			if a > self.value:
				a = self.value
			self.Health += a
			self.value -=a
		
	def HealthSub(self, a):
		if a > self.value:
			a = self.value

		if (self.Strengh - a) >= 0:
			self.Strengh -= a
			self.value +=a

	def AgilityAdd(self, a):
		if self.value > 0:
			if a > self.value:
				a = self.value
			self.Agility += a
			self.value -=a

	def AgilitySub(self, a):
		if a > self.value:
			a = self.value

		if (self.Strengh - a) >= 0:
			self.Strengh -= a
			self.value +=a

	def SapienceAdd(self, a):
		if self.value > 0:
			if a > self.value:
				a = self.value
			self.Sapience += a
			self.value -=a

	def SapienceSub(self, a):
		if a > self.value:
			a = self.value

		if (self.Strengh - a) >= 0:
			self.Strengh -= a
			self.value +=a

	def print_char(self):
		print("Ваш персонаж: ")
		print("Отсавшиеся очки %s" % self.value)
		print("Сила: %s" % self.Strengh)
		print("Здровье: %s" % self.Health)
		print("Ловкость: %s" % self.Agility)
		print("Мудрость: %s" % self.Sapience) 

Char = Character()

while True:
	select = input("Вы хотите добавить, отнять очки у характеристик персонажа или оставить всё как есть? ")
	if select == "Отнять" or select == "отнять":
		print("У какой характеристики вы хотите отнять очки?")
		Char.print_char()
		select = input()
		if select == "Сила" or select == "сила":
			val = int(input("Сколько очков вы хотите отнять? "))
			Char.StrenghSub(val)
		elif select == "Здоровье" or select == "здоровье":
			val = int(input("Сколько очков вы хотите отнять? "))
			Char.HealthSub(val)
		elif select == "Ловкость" or select == "ловкость":
			val = int(input("Сколько очков вы хотите отнять? "))
			Char.AgilitySub(val)
		elif select == "Мудрость" or select == "мудрость":
			val = int(input("Сколько очков вы хотите отнять? "))
			Char.SapienceSub(val)
		else:
			print("Неккоретный ввод")
		Char.print_char()
	elif select == "Добавить" or select == "добавить":
		print("Какую характеристику вы хотите усилить?")
		Char.print_char()
		select = input()
		if select == "Сила" or select == "сила":
			val = int(input("Сколько очков вы хотите добавить? "))
			Char.StrenghAdd(val)
		elif select == "Здоровье" or select == "здоровье":
			val = int(input("Сколько очков вы хотите добавить? "))
			Char.HealthAdd(val)
		elif select == "Ловкость" or select == "ловкость":
			val = int(input("Сколько очков вы хотите добавить? "))
			Char.AgilityAdd(val)
		elif select == "Мудрость" or select == "мудрость":
			val = int(input("Сколько очков вы хотите добавить? "))
			Char.SapienceAdd(val)
		else:
			print("Неккоретный ввод")
		Char.print_char()
	elif select == "оставить всё как есть" or select == "Оставить всё как есть":
		Char.print_char()
		break
	else:
		print("Неккоретный ввод")